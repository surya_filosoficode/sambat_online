package com.example.sambat.User_directory;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.sambat.LoginActivity;
import com.example.sambat.R;
import com.example.sambat.VerifikasiActivity;
import com.example.sambat.internal_library.Config.SessionCheck;

public class UserMain extends AppCompatActivity {

    TextView nama, email, alamat, profesi, no_telpon, username;

    Button btn_edit, btn_edit_pass;

    String id_user, logged_in;
    String TAG = "UserMain";

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_main);

        nama        = (TextView) findViewById(R.id.et_nama);
        email       = (TextView) findViewById(R.id.et_email);
        alamat      = (TextView) findViewById(R.id.et_alamat);
        profesi     = (TextView) findViewById(R.id.et_profesi);
        no_telpon   = (TextView) findViewById(R.id.et_no_telpon);

        btn_edit    = (Button) findViewById(R.id.btn_update);
        btn_edit_pass    = (Button) findViewById(R.id.btn_ch_pass);
//        username    = (TextView) findViewById(R.id.et_username);

        get_data();

        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(UserMain.this, UserEdit.class);
                startActivity(intent);
//                finish();
            }
        });

        btn_edit_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(UserMain.this, UserChangePass.class);
                startActivity(intent);
            }
        });

    }


    public void get_data() {
        String[] data_session = new SessionCheck().seesionLoginChecker(UserMain.this);
        Log.e(TAG, "onClick: " + data_session[1]);
        if (data_session != null) {

            String id_user_     = data_session[1];
            String nama_        = data_session[2];
            String email_       = data_session[3];
            String alamat_      = data_session[4];
            String profesi_     = data_session[5];
            String no_telpon_   = data_session[6];
            String username_    = data_session[7];

            nama.setText(nama_);
            email.setText(email_);
            alamat.setText(alamat_);
            profesi.setText(profesi_);
            no_telpon.setText(no_telpon_);
//            username.setText(username_);
        }
    }
}