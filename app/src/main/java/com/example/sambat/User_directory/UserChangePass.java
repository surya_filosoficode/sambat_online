package com.example.sambat.User_directory;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sambat.LoginActivity;
import com.example.sambat.R;
import com.example.sambat.RegisterMain;
import com.example.sambat.global_variable.MessageVariabe;
import com.example.sambat.global_variable.URLCollection;
import com.example.sambat.internal_library.Check_library.CheckConn;
import com.example.sambat.internal_library.Config.Retrofit_client;
import com.example.sambat.internal_library.Config.SessionCheck;
import com.example.sambat.internal_library.Retrofit_model.Register_request;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserChangePass extends AppCompatActivity {
    EditText et_pass, et_repass;
    Button btn_edit;

    String admin_auth = "admin", password_auth = "1234", id_user;
    String TAG = "UserChangePass";
    String url_sambat = URLCollection.SAMBAT_MAIN_URL;

    ProgressDialog progressDialog;
    MessageVariabe messageVariabe = new MessageVariabe();
    Intent intent;

    Register_request register_request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_change_password);

        et_pass      = (EditText) findViewById(R.id.et_password);
        et_repass    = (EditText) findViewById(R.id.et_repassword);

        btn_edit  = (Button) findViewById(R.id.btn_edit);

        get_data();

        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                progressDialog = new ProgressDialog(UserChangePass.this) {
                    @Override
                    public void onBackPressed() {
                        finish();
                    }
                };

                progressDialog.setMessage(messageVariabe.MESSAGE_LOADING);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setCancelable(false);
                progressDialog.show();

                String pass     = et_pass.getText().toString();
                final String repass   = et_repass.getText().toString();

                String set_auth = set_auth(admin_auth, password_auth);

                if(new CheckConn().isConnected(UserChangePass.this)){
                    if(admin_auth == "" || password_auth == ""){
                        Log.e(TAG, "input salah, ada yang kosong");
                    }else {

                        if(pass.equals(repass)){
                            RequestBody password_request    = RequestBody.create(MediaType.parse("multipart/form-data"), pass);
                            RequestBody id_user_request     = RequestBody.create(MediaType.parse("multipart/form-data"), id_user);

                            register_request = Retrofit_client.getClient_https(url_sambat).create(Register_request.class);
                            Call<ResponseBody> getdata = register_request.ch_pass_activity(set_auth, password_request, id_user_request);

                            getdata.enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    progressDialog.dismiss();
                                    try {
                                        Log.e(TAG, "onResponse: " + response.body().string());
//                                    JSONObject data_response;
//                                    try {
//                                        data_response = new JSONObject(response.body().string());
//                                        Log.e(TAG, "btn_login : data_response : "+ data_response);
//
//                                        Boolean status = data_response.getBoolean("status");
//
//                                        if(status){
//                                            intent = new Intent(RegisterMain.this, LoginActivity.class);
//                                            startActivity(intent);
//                                            finish();
//                                            Toast.makeText(RegisterMain.this, messageVariabe.MESSAGE_SUCCESS_REGISTER,
//                                                    Toast.LENGTH_LONG).show();
//                                        }else {
//                                            Toast.makeText(RegisterMain.this, messageVariabe.MESSAGE_ERROR_REGISTER,
//                                                    Toast.LENGTH_LONG).show();
//                                        }
//
//                                    } catch (JSONException e) {
//                                        Log.e(TAG, "btn_login : JSONException : "+ e.toString());
//                                    } catch (IOException e) {
//                                        Log.e(TAG, "btn_login : IOException :"+ e.toString());
//                                    }

                                    }catch (Exception e){
                                        Toast.makeText(UserChangePass.this, messageVariabe.MESSAGE_ERROR_REGISTER,
                                                Toast.LENGTH_LONG).show();
                                    }

                                }

                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    Toast.makeText(UserChangePass.this, messageVariabe.MESSAGE_ERROR_REGISTER,
                                            Toast.LENGTH_LONG).show();
                                    Log.e(TAG, "onFailure: " + t);
                                    progressDialog.dismiss();
                                }
                            });
                        }else {
                            Toast.makeText(UserChangePass.this, messageVariabe.MESSAGE_ERROR_REGISTER,
                                    Toast.LENGTH_LONG).show();
                            Log.e(TAG, "onFailure: password tidak sama");
                            progressDialog.dismiss();
                        }
                    }
                }else {
                    Log.e(TAG, "tidak ada koneksi");
                    Toast.makeText(UserChangePass.this, messageVariabe.MESSAGE_ERROR_REGISTER,
                            Toast.LENGTH_LONG).show();
                    progressDialog.dismiss();
                }

            }
        });
    }

    public static String set_auth(String admin_auth, String password_auth) {
        String base = admin_auth +":"+ password_auth;
        String auth_header = "Basic "+ Base64.encodeToString(base.getBytes(), Base64.NO_WRAP);

        return auth_header;
    }

    public void get_data() {
        String[] data_session = new SessionCheck().seesionLoginChecker(UserChangePass.this);
        Log.e(TAG, "onClick: " + data_session[1]);
        if (data_session != null) {

            String id_user_     = data_session[1];

            id_user = id_user_;
        }
    }
}
